# build stage
FROM node:14.15.5-slim as builder

WORKDIR /app
# CI helps with installing in dockerfiles
ENV CI true

COPY package.json .
COPY package-lock.json .

RUN npm install


# app stage
FROM node:14.15.5-slim as app

WORKDIR /app

COPY --from=builder /app/node_modules /app/node_modules
COPY . .

CMD ["npm", "run", "start"]
