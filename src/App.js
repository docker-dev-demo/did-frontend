import logo from './logo.svg';
import './App.css';
import AuthForm from './components/AuthForm';
import QRRenderer from './components/QRRenderer';

function App() {
  return (
    <div className="App">
      <header className="App-content">
        <img src={logo} className="App-logo" alt="logo" />
        <QRRenderer />
        <AuthForm />
      </header>
    </div>
  );
}

export default App;
