import { BACKEND_DOMAIN } from '../../config';
import React from 'react';
import './QRRenderer.css';

export default function QRRenderer() {
  const [qrdata, setQRData] = React.useState('');

  React.useEffect(() => {
    const getQRData = async () => {
      await fetch(`http://${BACKEND_DOMAIN}/gen-qrcode`)
        .then((response) => response.json())
        .then((data) => {
          setQRData(data['qrcode'][1]);
        });
    };
    getQRData();
  }, []);

  return (
    <div
      className="qr-container"
      dangerouslySetInnerHTML={{ __html: qrdata }}
    ></div>
  );
}
