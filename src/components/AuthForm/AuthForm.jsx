import { BACKEND_DOMAIN } from '../../config';
import React from 'react';
import Button from '../Button';
import ButtonRow from '../ButtonRow';
import InputField from '../InputField';

class LoginForm extends React.Component {
  constructor(state) {
    super(state);

    this.state = { code: null };

    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleChange(event) {
    this.setState({ code: event.target.value });
  }

  handleSubmit(event) {
    const code = this.state.code;

    if (!code) {
      alert('Must enter a code!');
      return;
    }

    fetch(`http://${BACKEND_DOMAIN}/check-qrcode`, {
      method: 'post',
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify({ code: code }),
    })
      .then((request) => request.json())
      .then((data) => {
        if (data['accepted'] === true) {
          alert('TOTP accepted!');
        } else {
          alert('TOTP not accepted!');
        }
      });

    event.preventDefault();
  }

  render() {
    return (
      <form onSubmit={this.handleSubmit}>
        <InputField
          id="totp"
          title="Authenticator Code"
          type="number"
          placeholder=" "
          onChange={this.handleChange}
        />
        <ButtonRow>
          <Button colorScheme="alt" text="Check" type="submit" />
        </ButtonRow>
      </form>
    );
  }
}

export default LoginForm;
