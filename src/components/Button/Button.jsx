import './Button.css';

export default function Button({ text, type, colorScheme, onClick }) {
  return (
    <button
      type={type ? type : 'button'}
      className={`Button ${colorScheme ? colorScheme : ''}`}
      onClick={onClick}
    >
      {text ? text : 'button'}
    </button>
  );
}
