import './ButtonRow.css';

export default function ButtonRow({ direction, children }) {
  return (
    <div className={`ButtonRow ${direction ? direction : 'right'}`}>
      {children}
    </div>
  );
}
