import './InputField.css';

export default function InputField({ id, title, placeholder, type, onChange }) {
  return (
    <div className="InputFieldContainer">
      <label
        htmlFor={id ? id : 'default-input-field-id'}
        className="InputFieldLabel"
      >
        {title ? title : 'No Title'}
      </label>
      <input
        id={id ? id : 'default-input-field-id'}
        className="InputFieldInput"
        placeholder={placeholder ? placeholder : 'Input'}
        type={type ? type : 'text'}
        onChange={onChange}
      />
    </div>
  );
}
