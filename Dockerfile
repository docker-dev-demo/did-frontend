# build stage
FROM node:14.15.5-slim as builder

WORKDIR /app
# CI helps with installing in dockerfiles
ENV CI true

# install modules
COPY package.json .
COPY package-lock.json .
RUN npm ci --production --silent

# build the static source
COPY . .
RUN npm run build


# app stage
FROM nginx:1.19.10 as app

# copy over application source
COPY --from=builder /app/build /usr/share/nginx/html
# copy over server config
COPY ./nginx/nginx.conf /etc/nginx/conf.d/default.conf
